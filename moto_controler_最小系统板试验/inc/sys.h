#ifndef SYS_H
#define	SYS_H

#include "include_all.h"

#define BITBAND(addr,n) *(unsigned int *)((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(n<<2))

#define SYSCLK_72MHz	72000000

void SetSysClockTo72(void);

#endif
