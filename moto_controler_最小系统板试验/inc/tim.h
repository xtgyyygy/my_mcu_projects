#ifndef TIM_H
#define	TIM_H

#include "include_all.h"

#define PWM_FEQ				16000
#define PWM_DUTY_100	4096

#define PWM_CH_U_H	0x01
#define PWM_CH_U_L	0x02
#define PWM_CH_V_H	0x04
#define PWM_CH_V_L	0x08
#define PWM_CH_W_H	0x10
#define PWM_CH_W_L	0x20
#define PWM_CH_ALL	0X3F

void TIM1_Init( void );
void TIM1_Stop_PWM( unsigned char );
void TIM1_Set_PWM( unsigned char, unsigned int );

void TIM2_Init( void );
void TIM2_Clear( void );
void TIM2_Start( void );
void TIM2_Stop( void );
unsigned long TIM2_Get_Time( void );

#endif
