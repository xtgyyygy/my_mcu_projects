#ifndef MOTC_H
#define	MOTC_H

#include "include_all.h"

void MOS_Stop( void );

//H_PWM  L_ONģʽ
void MOS_UV_Open( void );
void MOS_UW_Open( void );
void MOS_VW_Open( void );
void MOS_VU_Open( void );
void MOS_WV_Open( void );
void MOS_WU_Open( void );
void Motc_Proc_Hall( unsigned char, unsigned char );
void Mot_Calc_Period( unsigned char );
unsigned short Mot_Calc_Speed( unsigned long );

#endif
