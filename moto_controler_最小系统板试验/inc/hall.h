#ifndef HALL_H
#define	HALL_H

#include "include_all.h"

#define HALL_STATE_NO_INIT			0
#define HALL_STATE_0						1
#define HALL_STATE_60						2
#define HALL_STATE_120					3
#define HALL_STATE_180					4
#define HALL_STATE_240					5
#define HALL_STATE_300					6
#define HALL_STATE_INVALID			255

#define MOT_RUN_CW							1			//顺时针方向
#define MOT_RUN_ACW							2			//逆时针方向

extern unsigned char vucHALL_STATE;

void Hall_Init( void );
void Hall_Proc( void );
void Hall_Detect( void );

#endif
