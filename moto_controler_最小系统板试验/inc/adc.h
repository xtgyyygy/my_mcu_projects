#ifndef ADC_H
#define	ADC_H

#include "include_all.h"

void ADC_Init( void );
void Potent_Measure( void );
void VBUS_Measure( void );

#endif
