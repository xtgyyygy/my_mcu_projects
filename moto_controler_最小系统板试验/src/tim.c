#include "tim.h"


/*
MAX SPEED:	6000r/min	->	100r/s	->	10ms/r	->	10000us/r
MIN SPEED:	60r/min	->	1r/s	->	1000ms/r	->	1000000us/r

speed LSB:	2^-3r/s

*/
#define MOT_MIN_SPD					480
#define MOT_MAX_SPD					48000

#define MOT_MAX_TIME				20

#define MOT_SPEED_NORMAL		0
#define MOT_SPEED_TOO_LOW		1
		

unsigned int TIM_COUNTER_MAX;

unsigned int 	vucTIM_50MS_COUNT;
unsigned char	vucMOT_SPD_STS;

void TIM1_Init( void )
{
	//端口初始化
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	GPIOA->ODR &=~ ( GPIO_ODR_ODR8 | GPIO_ODR_ODR9 | GPIO_ODR_ODR10 );
	GPIOA->CRH &=~ ( GPIO_CRH_CNF8_0 | GPIO_CRH_CNF9_0 | GPIO_CRH_CNF10_0 );
	GPIOA->CRH |= ( GPIO_CRH_CNF8_1 | GPIO_CRH_CNF9_1 | GPIO_CRH_CNF10_1 );	
	GPIOA->CRH &=~ ( GPIO_CRH_MODE8 | GPIO_CRH_MODE9 | GPIO_CRH_MODE10 );	
	GPIOA->CRH |= ( GPIO_CRH_MODE8_1 | GPIO_CRH_MODE9_1 | GPIO_CRH_MODE10_1 );

	RCC->APB2ENR|=RCC_APB2ENR_IOPBEN;
	GPIOB->ODR |= ( GPIO_ODR_ODR13 | GPIO_ODR_ODR14 | GPIO_ODR_ODR15 );
	GPIOB->CRH &=~ ( GPIO_CRH_CNF13_0 | GPIO_CRH_CNF14_0 | GPIO_CRH_CNF15_0 );
	GPIOB->CRH |= ( GPIO_CRH_CNF13_1 | GPIO_CRH_CNF14_1 | GPIO_CRH_CNF15_1 );	
	GPIOB->CRH &=~ ( GPIO_CRH_MODE13 | GPIO_CRH_MODE14 | GPIO_CRH_MODE15 );	
	GPIOB->CRH |= ( GPIO_CRH_MODE13_1 | GPIO_CRH_MODE14_1 | GPIO_CRH_MODE15_1 );

	//定时器初始化
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	TIM1->CR1 = 0;
	TIM1->CR2 = 0;
	
	TIM1->CR1 |= TIM_CR1_ARPE;
	TIM1->CR1 |= TIM_CR1_CMS;				//中央对齐，上溢+下溢中断

	TIM1->CR2 |= TIM_CR2_OIS1;			//空闲输出为0
	TIM1->CR2 |= TIM_CR2_OIS2;
	TIM1->CR2 |= TIM_CR2_OIS3;
	TIM1->CR2 &=~ TIM_CR2_OIS1N;
	TIM1->CR2 &=~ TIM_CR2_OIS2N;
	TIM1->CR2 &=~ TIM_CR2_OIS3N;
	
	TIM1->CCMR1 = 0;
	TIM1->CCMR1	|= TIM_CCMR1_OC1M_1;		//PWM模式1，小于值时为有效电平
	TIM1->CCMR1	|= TIM_CCMR1_OC1M_2;
	TIM1->CCMR1	|= TIM_CCMR1_OC1PE;			//预装载使能
	
	TIM1->CCMR1	|= TIM_CCMR1_OC2M_1;		//PWM模式1，小于值时为有效电平
	TIM1->CCMR1	|= TIM_CCMR1_OC2M_2;
	TIM1->CCMR1	|= TIM_CCMR1_OC2PE;			//预装载使能
	
	TIM1->CCMR2 = 0;
	TIM1->CCMR2	|= TIM_CCMR2_OC3M_1;		//PWM模式1，小于值时为有效电平
	TIM1->CCMR2	|= TIM_CCMR2_OC3M_2;
	TIM1->CCMR2	|= TIM_CCMR2_OC3PE;			//预装载使能
	
	TIM1->PSC = 0;			//  (1/72)us/tick
	TIM1->CNT = 0;
	TIM_COUNTER_MAX = (unsigned int)((unsigned long)SYSCLK_72MHz/(unsigned long)PWM_FEQ);
	TIM1->ARR = TIM_COUNTER_MAX ;		//62.5us   16KHz
	
	TIM1->CCER = 0;			//输出高定平有效,关闭所有输出
	TIM1->CCER &=~ TIM_CCER_CC1E;			//CCP1 enable
	TIM1->CCER &=~ TIM_CCER_CC2E;			//CCP1 enable
	TIM1->CCER &=~ TIM_CCER_CC3E;			//CCP1 enable
	TIM1->CCER &=~ TIM_CCER_CC1NE;		//CCP1 enable
	TIM1->CCER &=~ TIM_CCER_CC2NE;		//CCP1 enable
	TIM1->CCER &=~ TIM_CCER_CC3NE;		//CCP1 enable
	TIM1->CCR1 = 0;
	TIM1->CCR2 = 0;
	TIM1->CCR3 = 0;
	TIM1->CCR4 = 0;
	
	TIM1->CR1 |= TIM_CR1_CEN;				//定时器使能
	
	TIM1->BDTR = 0;
	TIM1->BDTR |= TIM_BDTR_MOE;			//主输出使能
//	TIM1->BDTR &=~ TIM_BDTR_BKP;		//刹车输入低电平有效
//	TIM1->BDTR |= TIM_BDTR_BKE;			//刹车输入使能
//	TIM1->BDTR &=~ TIM_BDTR_OSSR;		//空闲时禁止输出
//	TIM1->BDTR &=~ TIM_BDTR_OSSI;

	TIM1->DIER = 0;
	TIM1->DIER |= ( TIM_DIER_BIE | 			//刹车中断
									TIM_DIER_TIE | 			//触发中断
									TIM_DIER_UIE |			//更新中断
									TIM_DIER_COMIE | 		//COM中断
									TIM_DIER_CC1IE |		//比较中断
									TIM_DIER_CC2IE |
									TIM_DIER_CC3IE |
									TIM_DIER_CC4IE );			
//	NVIC_EnableIRQ(TIM1_BRK_IRQn);
//	NVIC_EnableIRQ(TIM1_UP_IRQn);
//	NVIC_EnableIRQ(TIM1_TRG_COM_IRQn);
//	NVIC_EnableIRQ(TIM1_CC_IRQn);
}


void TIM1_Stop_PWM( unsigned char mask )
{
	if( ( mask & PWM_CH_U_H ) != 0 )
	{
		TIM1->CCR1 = 0;
		TIM1->CCER &=~ TIM_CCER_CC1E;			
	}
	if( ( mask & PWM_CH_V_H ) != 0 )
	{
		TIM1->CCR2 = 0;
		TIM1->CCER &=~ TIM_CCER_CC2E;			
	}
	if( ( mask & PWM_CH_W_H ) != 0 )
	{
		TIM1->CCR3 = 0;
		TIM1->CCER &=~ TIM_CCER_CC3E;			
	}
	if( ( mask & PWM_CH_U_L ) != 0 )
	{
		TIM1->CCR1 = 0;
		TIM1->CCER &=~ TIM_CCER_CC1NE;			
	}
	if( ( mask & PWM_CH_V_L ) != 0 )
	{
		TIM1->CCR2 = 0;
		TIM1->CCER &=~ TIM_CCER_CC2NE;			
	}
	if( ( mask & PWM_CH_W_L ) != 0 )
	{
		TIM1->CCR3 = 0;
		TIM1->CCER &=~ TIM_CCER_CC3NE;			
	}
}


/*
根据通道设置占空比
占空比为0时，停止输出
占空比LSB:2^-12
*/
void TIM1_Set_PWM( unsigned char ch, unsigned int duty )
{
	unsigned int ss_tim_load_val;
	if( duty != 0)
	{
		ss_tim_load_val = (unsigned int)(((unsigned long)TIM_COUNTER_MAX * (unsigned long)duty) >> 12);		
	}
	else
	{
		ss_tim_load_val = 0;
	}	
	
	switch( ch )
	{
		case PWM_CH_U_H:
			if( ss_tim_load_val != 0 )
			{
				TIM1->CCR1 = ss_tim_load_val;
				TIM1->CCER |= TIM_CCER_CC1E;				
			}
			else
			{
				TIM1->CCR1 = 0;
				TIM1->CCER &=~ TIM_CCER_CC1E;				
			}
			break;
		case PWM_CH_V_H:
			if( ss_tim_load_val != 0 )
			{
				TIM1->CCR2 = ss_tim_load_val;
				TIM1->CCER |= TIM_CCER_CC2E;				
			}
			else
			{
				TIM1->CCR2 = 0;
				TIM1->CCER &=~ TIM_CCER_CC2E;				
			}			
			break;
		case PWM_CH_W_H:
			if( ss_tim_load_val != 0 )
			{
				TIM1->CCR3 = ss_tim_load_val;
				TIM1->CCER |= TIM_CCER_CC3E;				
			}
			else
			{
				TIM1->CCR3 = 0;
				TIM1->CCER &=~ TIM_CCER_CC3E;				
			}			
			break;
		case PWM_CH_U_L:
			if( ss_tim_load_val != 0 )
			{
				TIM1->CCR1 = ss_tim_load_val;
				TIM1->CCER |= TIM_CCER_CC1NE;				
			}
			else
			{
				TIM1->CCR1 = 0;
				TIM1->CCER &=~ TIM_CCER_CC1NE;				
			}			
			break;
		case PWM_CH_V_L:
			if( ss_tim_load_val != 0 )
			{
				TIM1->CCR2 = ss_tim_load_val;
				TIM1->CCER |= TIM_CCER_CC2NE;				
			}
			else
			{
				TIM1->CCR2 = 0;
				TIM1->CCER &=~ TIM_CCER_CC2NE;				
			}			
			break;
		case PWM_CH_W_L:
			if( ss_tim_load_val != 0 )
			{
				TIM1->CCR3 = ss_tim_load_val;
				TIM1->CCER |= TIM_CCER_CC3NE;				
			}
			else
			{
				TIM1->CCR3 = 0;
				TIM1->CCER &=~ TIM_CCER_CC3NE;				
			}			
			break;
	}
}

void TIM1_UP_IRQHandler()
{
	TIM1->SR&=~TIM_SR_UIF;
}
void TIM1_CC_IRQHandler()
{
	TIM1->SR&=~TIM_SR_CC1IF;
}
void TIM1_TRG_COM_IRQHandler()
{
//	TIM1->SR&=~TIM_SR_CC1IF;
}
void TIM1_BRK_IRQHandler()
{
//	TIM1->SR&=~TIM_SR_UIF;
}


void TIM2_Init( void )
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	TIM2->CR1 = 0;
	TIM2->CR1 |= TIM_CR1_ARPE;
	TIM2->DIER |= TIM_DIER_UIE;
	TIM2->PSC = 72;						//定时期时钟72MHz
	TIM2->CNT = 0;
	TIM2->ARR = 50000;//25ms	1us/tick
	NVIC_EnableIRQ( TIM2_IRQn );
}

void TIM2_Clear( void )
{
	vucTIM_50MS_COUNT = 0;
	TIM2->CNT = 0;
	vucMOT_SPD_STS = MOT_SPEED_NORMAL;
}

void TIM2_Start( void )
{
	TIM2->CR1 |= TIM_CR1_CEN;
}

void TIM2_Stop( void )
{
	TIM2->CR1 &=~ TIM_CR1_CEN;
}

//return us
unsigned long TIM2_Get_Time( void )
{
	unsigned long time_us;
	time_us = (unsigned long)TIM2->CNT + 
						(unsigned long)vucTIM_50MS_COUNT * (unsigned long)50000;
	return time_us;
}


void TIM2_IRQHandler()
{
	TIM2->SR&=~TIM_SR_UIF;
	if( vucTIM_50MS_COUNT < MOT_MAX_TIME )
	{
		vucTIM_50MS_COUNT++;
		vucMOT_SPD_STS = MOT_SPEED_NORMAL;
	}
	else
	{
		vucMOT_SPD_STS = MOT_SPEED_TOO_LOW;		
	}

}
