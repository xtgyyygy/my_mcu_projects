#include "motc.h"

#define		ZSPEED_CALC_US		60000000

typedef struct TAG_MOT_PERIOD_LPF
{
	unsigned long	ul_Y0;
	unsigned short	us_lpf_coef;
}TAG_MOT_PERIOD_LPF;

unsigned int vssDUTY = 1024;
unsigned long vulPERIOD_TIME;
TAG_MOT_PERIOD_LPF	tagMOT_PERIOD_LPF;

void Mot_Init( void )
{
	tagMOT_PERIOD_LPF.ul_Y0 = 0;
	tagMOT_PERIOD_LPF.us_lpf_coef = 23429;
}

void MOS_Stop( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );
}

void MOS_UV_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );
	
	TIM1_Set_PWM( PWM_CH_U_H, vssDUTY );
	TIM1_Set_PWM( PWM_CH_V_L, PWM_DUTY_100 );
}

void MOS_UW_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );
	
	TIM1_Set_PWM( PWM_CH_U_H, PWM_DUTY_100 );
	TIM1_Set_PWM( PWM_CH_W_L, vssDUTY );
}

void MOS_VW_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );

	TIM1_Set_PWM( PWM_CH_V_H, vssDUTY );
	TIM1_Set_PWM( PWM_CH_W_L, PWM_DUTY_100 );
}

void MOS_VU_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );

	TIM1_Set_PWM( PWM_CH_V_H, PWM_DUTY_100 );
	TIM1_Set_PWM( PWM_CH_U_L, vssDUTY );
}

void MOS_WV_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );

	
	TIM1_Set_PWM( PWM_CH_W_H, PWM_DUTY_100 );
	TIM1_Set_PWM( PWM_CH_V_L, vssDUTY );
}

void MOS_WU_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );

	TIM1_Set_PWM( PWM_CH_W_H, vssDUTY );
	TIM1_Set_PWM( PWM_CH_U_L, PWM_DUTY_100 );
}

//根据霍尔传感器和旋转方向进行通电
void Motc_Proc_Hall( unsigned char hall_state, unsigned char dir )
{
	switch ( hall_state )
	{
		case HALL_STATE_0:
			if( dir == MOT_RUN_CW )
			{
				MOS_WV_Open();
			}
			else if( dir == MOT_RUN_ACW )
			{
				MOS_UW_Open();
			}
			break;
		case HALL_STATE_60:
			if( dir == MOT_RUN_CW )
			{
				MOS_UV_Open();
			}
			else if( dir == MOT_RUN_ACW )
			{
				MOS_VW_Open();
			}
			break;
		case HALL_STATE_120:
			if( dir == MOT_RUN_CW )
			{
				MOS_UW_Open();
			}
			else if( dir == MOT_RUN_ACW )
			{
				MOS_VU_Open();
			}
			break;
		case HALL_STATE_180:
			if( dir == MOT_RUN_CW )
			{
				MOS_VW_Open();
			}
			else if( dir == MOT_RUN_ACW )
			{
				MOS_WU_Open();
			}			
			break;
		case HALL_STATE_240:
			if( dir == MOT_RUN_CW )
			{
				MOS_VU_Open();
			}
			else if( dir == MOT_RUN_ACW )
			{
				MOS_WV_Open();
			}			
			break;
		case HALL_STATE_300:
			if( dir == MOT_RUN_CW )
			{
				MOS_WU_Open();
			}
			else if( dir == MOT_RUN_ACW )
			{
				MOS_UV_Open();
			}			
			break;
	}
}

void Mot_Calc_Period( unsigned char uc_hall_state )
{
	if( uc_hall_state == HALL_STATE_0 )
	{
		vulPERIOD_TIME = TIM2_Get_Time();
		TIM2_Clear();
	}
}

/*
speed 2^-3		MAX:7000rpm		MIN:60rpm
7000rpm		116.666667rps		8571.42us/r
*/
unsigned short Mot_Calc_Speed( unsigned long ul_period )
{
	unsigned long ul_speed;
	ul_speed = (unsigned long)ZSPEED_CALC_US / ul_period;
	
	return ( (unsigned short)ul_speed );
}

unsigned long Mot_Calc_TimeLPF( TAG_MOT_PERIOD_LPF *lpf, unsigned long ul_indat )
{
	long sl_buff;
	
	sl_buff = (long)ul_indat * (long)lpf->us_lpf_coef;
	sl_buff = ( (long)lpf->ul_Y0 - sl_buff ) >> 15;
	lpf->ul_Y0 = (unsigned long)( (long)lpf->ul_Y0 + sl_buff );
	
	return ( lpf->ul_Y0 );
}
