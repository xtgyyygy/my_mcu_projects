#include "led.h"

#define LED_OUT		BITBAND(GPIOC_BASE+0x0c,13)

void LED_Init( void )
{
	RCC->APB2ENR|=RCC_APB2ENR_IOPCEN;
	GPIOC->CRH&=~GPIO_CRH_CNF13_1;
	GPIOC->CRH&=~GPIO_CRH_CNF13_0;
	GPIOC->ODR|=GPIO_ODR_ODR13;
	GPIOC->CRH|=GPIO_CRH_MODE13_1;	
}	


void LED_ON( void )
{
	LED_OUT = 0;
}


void LED_OFF( void )
{
	LED_OUT = 1;
}

void LED_TOGGLE( void )
{
	if( LED_OUT != 0 )
	{
		LED_OUT = 0;
	}
	else
	{
		LED_OUT = 1;
	}
}
