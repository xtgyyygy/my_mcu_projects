#include "led.h"

#define LED_OUT		BITBAND(GPIOB_BASE+0x0c,2)

void LED_Init( void )
{
	RCC->APB2ENR|=RCC_APB2ENR_IOPBEN;
	GPIOB->CRL&=~GPIO_CRL_CNF2_1;
	GPIOB->CRL&=~GPIO_CRL_CNF2_0;
	GPIOB->ODR|=GPIO_ODR_ODR2;
	GPIOB->CRL|=GPIO_CRL_MODE2_1;	
}	


void LED_ON( void )
{
	LED_OUT = 0;
}


void LED_OFF( void )
{
	LED_OUT = 1;
}

void LED_TOGGLE( void )
{
	if( LED_OUT != 0 )
	{
		LED_OUT = 0;
	}
	else
	{
		LED_OUT = 1;
	}
}
