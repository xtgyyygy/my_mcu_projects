#include "motc.h"

#define		ZSPEED_CALC_US		60000000


#define		ZMOT_TIME_VALID_1			1
#define		ZMOT_TIME_VALID_2			2
#define		ZMOT_TIME_VALID_3			3
#define		ZMOT_TIME_VALID_4			4
#define		ZMOT_TIME_VALID_5			5
#define		ZMOT_TIME_VALID_6			6

//霍尔传感器
TAG_TIME_RECORD vtgMOT_TIME[7];

TAG_SIMPLE_LPF	tagMOT_PERIOD_LPF;


unsigned int vssDUTY = 2048;
unsigned long vulPERIOD_TIME;
unsigned long vulMOTO_SPEED;


void Mot_Init( void )
{
	tagMOT_PERIOD_LPF.ul_Y0 = 0;
	tagMOT_PERIOD_LPF.us_lpf_coef = 13287;		//200
}

void MOS_Stop( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );
}

void MOS_UV_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );
	
	TIM1_Set_PWM( PWM_CH_U_H, vssDUTY );
	TIM1_Set_PWM( PWM_CH_V_L, PWM_DUTY_100 );
}

void MOS_UW_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );
	
	TIM1_Set_PWM( PWM_CH_U_H, PWM_DUTY_100 );
	TIM1_Set_PWM( PWM_CH_W_L, vssDUTY );
}

void MOS_VW_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );

	TIM1_Set_PWM( PWM_CH_V_H, vssDUTY );
	TIM1_Set_PWM( PWM_CH_W_L, PWM_DUTY_100 );
}

void MOS_VU_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );
	TIM1_Set_PWM( PWM_CH_W_H, 0 );

	TIM1_Set_PWM( PWM_CH_V_H, PWM_DUTY_100 );
	TIM1_Set_PWM( PWM_CH_U_L, vssDUTY );
}

void MOS_WV_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_L, 0 );
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );

	
	TIM1_Set_PWM( PWM_CH_W_H, PWM_DUTY_100 );
	TIM1_Set_PWM( PWM_CH_V_L, vssDUTY );
}

void MOS_WU_Open( void )
{
	TIM1_Set_PWM( PWM_CH_U_H, 0 );
	TIM1_Set_PWM( PWM_CH_V_L, 0 );
	TIM1_Set_PWM( PWM_CH_V_H, 0 );
	TIM1_Set_PWM( PWM_CH_W_L, 0 );

	TIM1_Set_PWM( PWM_CH_W_H, vssDUTY );
	TIM1_Set_PWM( PWM_CH_U_L, PWM_DUTY_100 );
}

//根据霍尔传感器和旋转方向进行通电
void Motc_Proc_Hall( unsigned char hall_state, unsigned char dir )
{
	switch ( hall_state )
	{
		case HALL_STATE_0:
			if( dir == MOT_RUN_CW )
			{
				MOS_WV_Open();
			}
			else if( dir == MOT_RUN_CCW )
			{
				MOS_UW_Open();
			}
			break;
		case HALL_STATE_60:
			if( dir == MOT_RUN_CW )
			{
				MOS_UV_Open();
			}
			else if( dir == MOT_RUN_CCW )
			{
				MOS_VW_Open();
			}
			break;
		case HALL_STATE_120:
			if( dir == MOT_RUN_CW )
			{
				MOS_UW_Open();
			}
			else if( dir == MOT_RUN_CCW )
			{
				MOS_VU_Open();
			}
			break;
		case HALL_STATE_180:
			if( dir == MOT_RUN_CW )
			{
				MOS_VW_Open();
			}
			else if( dir == MOT_RUN_CCW )
			{
				MOS_WU_Open();
			}			
			break;
		case HALL_STATE_240:
			if( dir == MOT_RUN_CW )
			{
				MOS_VU_Open();
			}
			else if( dir == MOT_RUN_CCW )
			{
				MOS_WV_Open();
			}			
			break;
		case HALL_STATE_300:
			if( dir == MOT_RUN_CW )
			{
				MOS_WU_Open();
			}
			else if( dir == MOT_RUN_CCW )
			{
				MOS_UV_Open();
			}			
			break;
	}
}

/*
speed 2^-3		MAX:7000rpm		MIN:60rpm
7000rpm		116.666667rps		8571.42us/r
*/

unsigned long Mot_Calc_Speed( unsigned char uc_hall_state )
{
	unsigned long ul_speed;
	static unsigned long ul_period;
	unsigned long ul_period_lpf;
	
	TIM2_Get_Time(&vtgMOT_TIME[0]);

	TIM2_Clear_Timeout_Cnt( TIMOUT_EVENT_SPEED );

	switch( uc_hall_state )
	{
		case HALL_STATE_0:
			vtgMOT_TIME[1].ulTIME_50MS_CNT_PRE = vtgMOT_TIME[1].ulTIME_50MS_CNT;
			vtgMOT_TIME[1].ulTIME_50MS_CNT	= vtgMOT_TIME[0].ulTIME_50MS_CNT;
			vtgMOT_TIME[1].ulTIME_US_PRE = vtgMOT_TIME[1].ulTIME_US;
			vtgMOT_TIME[1].ulTIME_US	= vtgMOT_TIME[0].ulTIME_US;
			ul_period = TIM2_Calc_Delta_Time( vtgMOT_TIME[1] );
			break;
		case HALL_STATE_60:
			vtgMOT_TIME[2].ulTIME_50MS_CNT_PRE = vtgMOT_TIME[2].ulTIME_50MS_CNT;
			vtgMOT_TIME[2].ulTIME_50MS_CNT	= vtgMOT_TIME[0].ulTIME_50MS_CNT;
			vtgMOT_TIME[2].ulTIME_US_PRE = vtgMOT_TIME[2].ulTIME_US;
			vtgMOT_TIME[2].ulTIME_US	= vtgMOT_TIME[0].ulTIME_US;
			ul_period = TIM2_Calc_Delta_Time( vtgMOT_TIME[2] );
			break;
		case HALL_STATE_120:
			vtgMOT_TIME[3].ulTIME_50MS_CNT_PRE = vtgMOT_TIME[3].ulTIME_50MS_CNT;
			vtgMOT_TIME[3].ulTIME_50MS_CNT	= vtgMOT_TIME[0].ulTIME_50MS_CNT;
			vtgMOT_TIME[3].ulTIME_US_PRE = vtgMOT_TIME[3].ulTIME_US;
			vtgMOT_TIME[3].ulTIME_US	= vtgMOT_TIME[0].ulTIME_US;
			ul_period = TIM2_Calc_Delta_Time( vtgMOT_TIME[3] );
			break;
		case HALL_STATE_180:
			vtgMOT_TIME[4].ulTIME_50MS_CNT_PRE = vtgMOT_TIME[4].ulTIME_50MS_CNT;
			vtgMOT_TIME[4].ulTIME_50MS_CNT	= vtgMOT_TIME[0].ulTIME_50MS_CNT;
			vtgMOT_TIME[4].ulTIME_US_PRE = vtgMOT_TIME[4].ulTIME_US;
			vtgMOT_TIME[4].ulTIME_US	= vtgMOT_TIME[0].ulTIME_US;
			ul_period = TIM2_Calc_Delta_Time( vtgMOT_TIME[4] );
			break;
		case HALL_STATE_240:
			vtgMOT_TIME[5].ulTIME_50MS_CNT_PRE = vtgMOT_TIME[5].ulTIME_50MS_CNT;
			vtgMOT_TIME[5].ulTIME_50MS_CNT	= vtgMOT_TIME[0].ulTIME_50MS_CNT;
			vtgMOT_TIME[5].ulTIME_US_PRE = vtgMOT_TIME[5].ulTIME_US;
			vtgMOT_TIME[5].ulTIME_US	= vtgMOT_TIME[0].ulTIME_US;
			ul_period = TIM2_Calc_Delta_Time( vtgMOT_TIME[5] );
			break;
		case HALL_STATE_300:
			vtgMOT_TIME[6].ulTIME_50MS_CNT_PRE = vtgMOT_TIME[6].ulTIME_50MS_CNT;
			vtgMOT_TIME[6].ulTIME_50MS_CNT	= vtgMOT_TIME[0].ulTIME_50MS_CNT;
			vtgMOT_TIME[6].ulTIME_US_PRE = vtgMOT_TIME[6].ulTIME_US;
			vtgMOT_TIME[6].ulTIME_US	= vtgMOT_TIME[0].ulTIME_US;
			ul_period = TIM2_Calc_Delta_Time( vtgMOT_TIME[6] );
			break;
	}
	ul_period_lpf = If_Cfc_Calc_LPF( &tagMOT_PERIOD_LPF, ul_period );
	
	ul_speed = (unsigned long)ZSPEED_CALC_US / ul_period_lpf;
	vulMOTO_SPEED = ul_speed;

	return ( (unsigned long)ul_speed );
}


