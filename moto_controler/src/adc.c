#include "adc.h"
/*
规则通道：
ADC1: ID_C(CH0)、电位器(CH3)
ADC2: IV_U(CH2)、IV_V(CH1)
触发源：TIM1_CC1、TIM1_CC2、TIM1_CC3

注入通道：
ADC1: 电位器(CH3)
ADC2: 电源电压(CH8)
触发源：软件触发

1、在不启动电机时，使用注入通道转换电位器

*/

#define		ADC_EXTSEL_TIM1_CC1		0
#define		ADC_EXTSEL_TIM1_CC2		1
#define		ADC_EXTSEL_TIM1_CC3		2

void ADC_Init( void )
{
	//ADC模拟输入
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	GPIOA->CRL &=~ ( GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2 | GPIO_CRL_CNF3 );
	GPIOA->CRL &=~ ( GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3 );
	
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	RCC->APB2ENR |= RCC_APB2ENR_ADC2EN;
	
	ADC1->CR2 |= ADC_CR2_ADON;
	ADC2->CR2 |= ADC_CR2_ADON;
	delay_ms(5);
	
	/*** 校准ADC ***/
	ADC1->CR2 |= ADC_CR2_RSTCAL;					//复位校准寄存器
	ADC2->CR2 |= ADC_CR2_RSTCAL;	
	ADC1->CR2 |= ADC_CR2_CAL;							//校准
	ADC2->CR2 |= ADC_CR2_CAL;
	while( ( ADC1->CR2 & ADC_CR2_CAL ) || ( ADC1->CR2 & ADC_CR2_CAL ) );
	
	/*** 配置ADC 规则组 ***/
	ADC1->CR1 &=~ ADC_CR1_DUALMOD;				//同步规则模式
	ADC1->CR1 |= ( ADC_CR1_DUALMOD_1 | ADC_CR1_DUALMOD_2 );
	ADC1->CR1 &=~ ADC_CR1_DISCNUM;				//2个转换通道
	ADC1->CR1 |= ADC_CR1_DISCNUM_0;
	ADC2->CR1 &=~ ADC_CR1_DISCNUM;
	ADC2->CR1 |= ADC_CR1_DISCNUM_0;
	ADC1->CR1 |= ADC_CR1_DISCEN;					//间断模式
	ADC2->CR1 |= ADC_CR1_DISCEN;	
	ADC1->CR1 |= ADC_CR1_EOCIE;						//规则通道中断使能
	ADC2->CR1 |= ADC_CR1_EOCIE;
	ADC1->CR2 |= ADC_CR2_EXTTRIG;					//使能外部触发转换模式
	ADC2->CR2 |= ADC_CR2_EXTTRIG;
	ADC1->CR2 &=~ ADC_CR2_EXTSEL;					//定时器1的CC1事件触发
	ADC2->CR2 &=~ ADC_CR2_EXTSEL;
	ADC1->CR2 &=~ ADC_CR2_ALIGN;					//数据右对齐
	ADC2->CR2 &=~ ADC_CR2_ALIGN;
//	ADC1->CR2 |= ADC_CR2_ALIGN;						//DMA请求
//	ADC2->CR2 |= ADC_CR2_ALIGN;	
	ADC1->CR1 |= ADC_CR1_SCAN;						//扫描模式
	ADC2->CR1 |= ADC_CR1_SCAN;
	ADC1->SQR1 = 1<<20;										//ADC1:2个转换
	ADC2->SQR1 = 1<<20;										//ADC2:2个转换
	ADC1->SQR3 |= ( 0<<0 | 3<<5 );				//ADC1:CH0、CH3
	ADC2->SQR3 |= ( 1<<0 | 2<<5 );				//ADC1:CH1、CH2
	
	/*** 配置ADC 注入组 ***/	
	ADC1->CR1 |= ADC_CR1_JEOCIE;					//注入通道使能,电位器用
	ADC1->CR1 |= ADC_CR1_JDISCEN;					//注入通道间断模式
	ADC1->CR2 |= ADC_CR2_JEXTTRIG;				//外部触发模式
	ADC1->CR2 &=~ ADC_CR2_JEXTSEL;				//外部触发模式
	ADC1->CR2 |= ADC_CR2_JEXTSEL;					//外部触发模式:软件触发
	ADC1->JOFR1 &=~ ADC_JOFR1_JOFFSET1;		//注入通道数据偏移0
	ADC1->JSQR = 0<<20;										//ADC2:1个转换
	ADC1->JSQR |= 3<<15;									//ADC1:CH3(电位器)

	ADC2->CR1 |= ADC_CR1_JEOCIE;					//注入通道使能,电源电压用
	ADC2->CR1 |= ADC_CR1_JDISCEN;					//注入通道间断模式
	ADC2->CR2 |= ADC_CR2_JEXTTRIG;				//外部触发模式
	ADC2->CR2 &=~ ADC_CR2_JEXTSEL;				//外部触发模式
	ADC2->CR2 |= ADC_CR2_JEXTSEL;					//外部触发模式:软件触发
	ADC2->JOFR1 &=~ ADC_JOFR1_JOFFSET1;		//注入通道数据偏移0
	ADC2->JSQR = 0<<20;										//ADC2:1个转换
	ADC2->JSQR |= 8<<15;									//ADC2:CH8(V_BUS)

	//通用配置
	NVIC_EnableIRQ( ADC1_2_IRQn );
	ADC1->SMPR2 &=~ ( ADC_SMPR2_SMP0 | 		//采样时间239.5周期 
										ADC_SMPR2_SMP3 );
	ADC1->SMPR2 |= ( ADC_SMPR2_SMP0 | 
										ADC_SMPR2_SMP3 );	
	ADC2->SMPR2 &=~ ( ADC_SMPR2_SMP1 | 
										ADC_SMPR2_SMP2 );											
	ADC2->SMPR2 |= ( ADC_SMPR2_SMP1 | 
										ADC_SMPR2_SMP2 );	
}


void ADC_Triger_Change_CH( unsigned char ch )
{
	ADC1->CR2 &=~ ADC_CR2_EXTSEL;
	ADC1->CR2 |= ch<<17;
}


void Potent_Measure( void )
{
	ADC1->CR2 |= ADC_CR2_JSWSTART;
}


void VBUS_Measure( void )
{
	ADC2->CR2 |= ADC_CR2_JSWSTART;
}


void ADC1_2_IRQHandler( void )
{
	unsigned int temp;
	ADC1->SR = 0;
	ADC2->SR = 0;
	if(ADC1->DR)temp=temp;
	if(ADC2->DR)temp=temp;
	LED_TOGGLE();
}


void ADC_Start( void )
{
//	ADC1->CR2|=ADC_CR2_SWSTART;
	
//	dat=(ADC1->DR&0X0000FFFF);
}

//void ADC_Stop( void );
