#include "if_cfc.h"

// yn = lpf_coef * yn-1 + (1-coef)xn
//yn = coed * yn-1 + xn - coef * xn
//

unsigned long If_Cfc_Calc_LPF( TAG_SIMPLE_LPF *lpf, unsigned long ul_indat )
{
	long long dl_buff;
	
	dl_buff = ( (long long)lpf->ul_Y0 - (long long)ul_indat ) * (long long)lpf->us_lpf_coef;
	dl_buff = dl_buff>>15;
	dl_buff += (long long)ul_indat;
	lpf->ul_Y0 = (unsigned long)dl_buff;
	return ( lpf->ul_Y0 );
}

void If_Cfc_Memcpy( unsigned char *des, unsigned char *src, unsigned long bytes )
{
	while( bytes-- )
	{
		*des++ = *src++;
	}
}
