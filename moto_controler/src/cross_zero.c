#include "cross_zero.h"

#define CROSS_ZERO_CH_U		(1)
#define CROSS_ZERO_CH_V		(2)
#define CROSS_ZERO_CH_W		(3)

void Cross_Zero_Init( void )
{
	//过零检测三线，浮空输入（已有带硬件上拉）
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	GPIOB->CRL &=~ ( GPIO_CRL_CNF3 | GPIO_CRL_CNF4 );
	GPIOB->CRL |= ( GPIO_CRL_CNF3_0 | GPIO_CRL_CNF4_0 );
	GPIOB->CRL &=~ ( GPIO_CRL_MODE3 | GPIO_CRL_MODE4 );
	GPIOB->ODR |= ( GPIO_ODR_ODR3 | GPIO_ODR_ODR4 );

	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	GPIOA->CRH &=~ GPIO_CRH_CNF15;
	GPIOA->CRH |= GPIO_CRH_CNF15_0;
	GPIOA->CRH &=~ GPIO_CRH_MODE15;
	GPIOA->ODR |= GPIO_ODR_ODR15;
	
	//中断配置，上升沿和下降沿
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI3_PB;
	AFIO->EXTICR[1] |= AFIO_EXTICR2_EXTI4_PB;
	EXTI->RTSR |= ( EXTI_FTSR_TR3 | EXTI_FTSR_TR4 );
	EXTI->FTSR |= ( EXTI_FTSR_TR3 | EXTI_FTSR_TR4 );

	EXTI->IMR |= ( EXTI_IMR_MR3 | EXTI_IMR_MR4 );
	EXTI->EMR |= ( EXTI_EMR_MR3 | EXTI_EMR_MR4 );
	NVIC_EnableIRQ( EXTI3_IRQn );
	NVIC_EnableIRQ( EXTI4_IRQn );
	
	AFIO->EXTICR[3] |= AFIO_EXTICR4_EXTI15_PA;
	EXTI->RTSR |= EXTI_FTSR_TR15;
	EXTI->FTSR |= EXTI_FTSR_TR15;

	EXTI->IMR |= EXTI_IMR_MR15;	
	EXTI->EMR |= EXTI_EMR_MR15;
	NVIC_EnableIRQ( EXTI15_10_IRQn );	
}


void Cross_Zero_Proc( void )
{
	LED_TOGGLE();
}


/*
unsigned char Hall_Check( unsigned char ch )
{
	switch(ch)
	{
		case HALL_CH_HA:
			
			break;
		case HALL_CH_HB:
			
			break;
		case HALL_CH_HC:
			
			break;
	}
}*/
