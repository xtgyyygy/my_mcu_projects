#include "hall.h"

#define HALL_CH_HA		(1)
#define HALL_CH_HB		(2)
#define HALL_CH_HC		(3)

#define HALL_CH_HA_IN		BITBAND(GPIOC_BASE+0x08,13)
#define HALL_CH_HB_IN		BITBAND(GPIOC_BASE+0x08,14)
#define HALL_CH_HC_IN		BITBAND(GPIOC_BASE+0x08,15)

unsigned char vucHALL_STATE;
unsigned char vucHALL_STATE_PRE;

void Hall_Init( void )
{
	vucHALL_STATE = HALL_STATE_INVALID;
	vucHALL_STATE_PRE = HALL_STATE_INVALID;	
	
	//霍尔三线，浮空输入（已有带硬件上拉）
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	GPIOC->CRH &=~ ( GPIO_CRH_CNF13 | GPIO_CRH_CNF14 | GPIO_CRH_CNF15 );
	GPIOC->CRH |= ( GPIO_CRH_CNF13_0 | GPIO_CRH_CNF14_0 | GPIO_CRH_CNF15_0 );
	GPIOC->CRH &=~ ( GPIO_CRH_MODE13 | GPIO_CRH_MODE14 | GPIO_CRH_MODE15 );
	GPIOC->ODR |= ( GPIO_ODR_ODR13 | GPIO_ODR_ODR14 | GPIO_ODR_ODR15 );
	
	//中断配置，上升沿和下降沿
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	AFIO->EXTICR[3] |= ( AFIO_EXTICR4_EXTI13_PC | AFIO_EXTICR4_EXTI14_PC | AFIO_EXTICR4_EXTI15_PC );	//?????
	EXTI->RTSR |= ( EXTI_FTSR_TR13 | EXTI_FTSR_TR14 | EXTI_FTSR_TR15 );
	EXTI->FTSR |= ( EXTI_FTSR_TR13 | EXTI_FTSR_TR14 | EXTI_FTSR_TR15 );

	EXTI->IMR |= ( EXTI_IMR_MR13 | EXTI_IMR_MR14 | EXTI_IMR_MR15 );	
	EXTI->EMR |= ( EXTI_EMR_MR13 | EXTI_EMR_MR14 | EXTI_EMR_MR15 );		//??????IMR?EMR
	NVIC_EnableIRQ( EXTI15_10_IRQn );
}


void Hall_Detect( void )
{
	unsigned char uc_hall_state;

	if( HALL_CH_HA_IN == 1 &&
			HALL_CH_HB_IN == 0 &&
			HALL_CH_HC_IN == 1 )
	{
		uc_hall_state = HALL_STATE_0;
	}
	else if( HALL_CH_HA_IN == 1 &&
					 HALL_CH_HB_IN == 0 &&
					 HALL_CH_HC_IN == 0 )
	{
		uc_hall_state = HALL_STATE_60;	
	}
	else if( HALL_CH_HA_IN == 1 &&
					 HALL_CH_HB_IN == 1 &&
					 HALL_CH_HC_IN == 0 )
	{
		uc_hall_state = HALL_STATE_120;		
	}
	else if( HALL_CH_HA_IN == 0 &&
					 HALL_CH_HB_IN == 1 &&
					 HALL_CH_HC_IN == 0 )
	{
		uc_hall_state = HALL_STATE_180;		
	}
	else if( HALL_CH_HA_IN == 0 &&
					 HALL_CH_HB_IN == 1 &&
					 HALL_CH_HC_IN == 1 )
	{
		uc_hall_state = HALL_STATE_240;		
	}
	else if( HALL_CH_HA_IN == 0 &&
					 HALL_CH_HB_IN == 0 &&
					 HALL_CH_HC_IN == 1 )
	{
		uc_hall_state = HALL_STATE_300;		
	}
	if ( vucHALL_STATE != uc_hall_state )
	{
		vucHALL_STATE_PRE = vucHALL_STATE;
		vucHALL_STATE = uc_hall_state;
	}
}

void Hall_Proc( void )
{
	Hall_Detect();
}
