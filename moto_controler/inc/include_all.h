#ifndef INCLUDE_ALL_H
#define	INCLUDE_ALL_H

#include "stm32f10x.h"
#include "sys.h"
#include "if_cfc.h"
#include "delay.h"
#include "led.h"
#include "hall.h"
#include "cross_zero.h"
#include "adc.h"
#include "motc.h"
#include "tim.h"

#endif



