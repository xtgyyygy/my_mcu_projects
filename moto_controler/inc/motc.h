#ifndef MOTC_H
#define	MOTC_H

#include "include_all.h"

//马达速度	LSB:r/min
extern unsigned long vulMOTO_SPEED;

void MOS_Stop( void );
//PWM_ON
void MOS_UV_Open( void );
void MOS_UW_Open( void );
void MOS_VW_Open( void );
void MOS_VU_Open( void );
void MOS_WV_Open( void );
void MOS_WU_Open( void );
void Motc_Proc_Hall( unsigned char, unsigned char );
unsigned long Mot_Calc_Speed( unsigned char );

#endif
