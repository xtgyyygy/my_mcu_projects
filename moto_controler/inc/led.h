#ifndef LED_H
#define	LED_H

#include "include_all.h"

void LED_Init( void );
void LED_ON( void );
void LED_OFF( void );
void LED_TOGGLE( void );

#endif
