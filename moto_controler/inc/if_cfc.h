#ifndef IF_CFC_H
#define	IF_CFC_H

#include "include_all.h"

typedef struct TAG_SIMPLE_LPF
{
	unsigned long	ul_Y0;
	unsigned short	us_lpf_coef;
}TAG_SIMPLE_LPF;

unsigned long If_Cfc_Calc_LPF( TAG_SIMPLE_LPF*, unsigned long );
void If_Cfc_Memcpy( unsigned char*, unsigned char*, unsigned long );

#endif
