#ifndef DELAY_H
#define	DELAY_H

#include "include_all.h"

void delay_us( unsigned int );
void delay_ms( unsigned int );


#endif
