#ifndef TIM_H
#define	TIM_H

#include "include_all.h"

#define PWM_FEQ				16000
#define PWM_DUTY_100	4096

#define PWM_CH_U_H	0x01
#define PWM_CH_U_L	0x02
#define PWM_CH_V_H	0x04
#define PWM_CH_V_L	0x08
#define PWM_CH_W_H	0x10
#define PWM_CH_W_L	0x20
#define PWM_CH_ALL	0X3F

#define TIMOUT_EVENT_CNT			2

#define TIMOUT_EVENT_SPEED		0
#define TIMOUT_EVENT_DEFAULT	1

typedef struct TAG_TIME_RECORD
{
	unsigned long ulTIME_50MS_CNT;
	unsigned long ulTIME_50MS_CNT_PRE;
	unsigned long ulTIME_US;
	unsigned long ulTIME_US_PRE;
}TAG_TIME_RECORD;

typedef struct TAG_TIMEOUT_EVENT
{
	unsigned long ul_time_set;
	unsigned long ul_time_count;
	void (*timout_func)( void );
}TAG_TIMEOUT_EVENT;

void TIM1_Init( void );
void TIM1_Stop_PWM( unsigned char );
void TIM1_Set_PWM( unsigned char, unsigned int );

void TIM2_Init( void );
void TIM2_Clear( void );
void TIM2_Start( void );
void TIM2_Stop( void );
void TIM2_Get_Time( TAG_TIME_RECORD* );
unsigned long TIM2_Calc_Delta_Time( TAG_TIME_RECORD );
void TIM2_Proc_Timeout( void );
void TIM2_Clear_Timeout_Cnt( unsigned char );

void Timout_Event_0( void );
void Timout_Event_1( void );

#endif
